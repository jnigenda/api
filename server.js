//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
//3000
var path = require('path');
var movimientosV2JSON = require('./movimientosv2.json');
var bodyparser = require('body-parser');

var requestjson = require('request-json');
var bcrypt = require('bcrypt');
var dateFormat = require('dateformat');
var BCRYPT_SALT_ROUNDS = 12;
//const mongoose = require('mongoose');

var urlClientes = "https://api.mlab.com/api/1/databases/jnigenda/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlUsuarios = "https://api.mlab.com/api/1/databases/jnigenda/collections/Usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLab = requestjson.createClient(urlClientes);
var usuarioMLab = requestjson.createClient(urlUsuarios);

var urlMLabRaiz = "https://api.mlab.com/api/1/databases/jnigenda/collections/";

var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLabRaiz;
var usuarioMLabRaiz;

app.use(bodyparser.json());

app.use(function (req, res, next){
  res.header('Access-Control-Allow-Origin', ['*']);
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  //res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
})

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

// POST LOGIN
app.post('/v1/login',function(req, res){

  var validaCampos = true;
  var mensajeValida = '';
  if (!req.body.email || !req.body.password){
    validaCampos = false;
    mensajeValida = 'El dato del email y password son requeridos.';
  }else {
    if (req.body.email === '' || req.body.password === ''){
      validaCampos = false;
      mensajeValida = 'El dato del email o password no puede ser vacio.';
    }
  }
  if (!validaCampos){
    res.status(404).send({error: true, mensaje:mensajeValida});
  }else{
  var email = req.body.email;
  var password = req.body.password;
  var queryEmail = 'q={"email":"' + email +'"}';

    clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Usuarios?" + apiKey + "&" + queryEmail);
    clienteMLabRaiz.get('',function (err, resM, body) {
    if (!err){
      if (body.length == 1){
        if(bcrypt.compareSync(password, body[0].password)){
          res.status(200).send({error: false, nombre: body[0].nombre, idCliente: body[0].idCliente,  mensaje: 'Usuario logado.'});
        }else {
          res.status(400).send({error: true,  mensaje: 'Password incorrecto.'});
        }
      }else {
        res.status(400).send({error: true, mensaje: 'El usuario no existe, Regístrese.'});
      }
    }else {
      res.status(404).send({error: true, mensaje: 'Ocurrio un error al consultar el usuario.' +body});
    }
  })
}

})

// POST -- USUARIO ALTA
app.post('/v1/usuarios', function(req, res){
  var validaCampos = true;
  var mensajeValida = '';
  if (!req.body.email || !req.body.password){
    validaCampos = false;
    mensajeValida = 'El dato del email y password son requeridos.';
  }else {
    if (req.body.email === '' || req.body.password === ''){
      validaCampos = false;
      mensajeValida = 'El dato del email o password no puede ser vacio.';
    }else{
      if (!req.body.nombre || !req.body.apellidoPaterno || !req.body.apellidoMaterno){
        validaCampos = false;
        mensajeValida = 'La informacion del cliente es requerida.';
      }else {
        if (!req.body.idCuenta || !req.body.monto){
          validaCampos = false;
          mensajeValida = 'La informacion de la cuenta es requerida.';
        }
      }
    }
  }

if (!validaCampos){
  res.status(404).send({error: true, mensaje:mensajeValida});
}else{
  //Validar que no exista el usuario
  var queryUsuario = 'q={"email":"' + req.body.email +'"}';
  var clienteMLabRaizUsuario = requestjson.createClient(urlMLabRaiz + "Usuarios?" + apiKey + "&" + queryUsuario);
  clienteMLabRaizUsuario.get('', function(errUsuario, resUsuario, bodyUsuario){
    if(!errUsuario){
      if(bodyUsuario.length >= 1){
        res.status(400).send({error: true, mensaje:"El usuario ya existe."});
      }
      else{
        //Validar que la cuenta NO pertenezca a otro usuario
        var queryCuenta = 'q={"idCuenta":' + req.body.idCuenta +'}';
        clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Cuentas?" + apiKey + "&" + queryCuenta);
        clienteMLabRaiz.get('', function(errCuenta, resCuenta, bodyCuenta){
          if(!errCuenta){
            if(bodyCuenta.length >= 1){
              res.status(400).send({error: true, mensaje:"La cuenta ya ésta relacionada a otro usuario."});
            }
            else{
              var queryUsuarios= 's={"idCliente":-1}&l=1';
              var idClienteNuevo = 0;
              //Calcular el IdCliente
              var clienteMLabRaizUsuarios = requestjson.createClient(urlMLabRaiz + "Usuarios?" + apiKey + "&" + queryUsuarios);
              clienteMLabRaizUsuarios.get('',function(errUsuarios,resUsuarios,bodyUsuarios){
                if(!errUsuarios){
                  if (bodyUsuarios[0] == null){
                    idClienteNuevo =  1;
                  }else{
                    if (bodyUsuarios[0].idCliente == null){
                      idClienteNuevo =  1;
                    }else{
                      idClienteNuevo= bodyUsuarios[0].idCliente + 1;
                    }
                  }
                  bcrypt.hash(req.body.password, BCRYPT_SALT_ROUNDS, function(err, hash) {
                    var jsonInsertaUsuario = '{"email":"' + req.body.email + '", "password":"' + hash + '", "idCliente":' + idClienteNuevo + ',"nombre":"' + req.body.nombre +'","apellidoPaterno":"' + req.body.apellidoPaterno  + '","apellidoMaterno":"' + req.body.apellidoMaterno + '"}';
                    // POST Insert Usuario
                    var clienteMLabRaizInsertaUsuario = requestjson.createClient(urlMLabRaiz + "Usuarios?" + apiKey);
                    clienteMLabRaizInsertaUsuario.post('', JSON.parse(jsonInsertaUsuario), function(errInsertaUsuario, resInsertaUsuario, bodyInsertaUsuario){
                      if(!errInsertaUsuario){
                            var jsonCuenta = '{"idCuenta":' + req.body.idCuenta +',"monto":' + req.body.monto + ',"idCliente":' + idClienteNuevo + '}';
                            //POST Insert Cuenta del usuario
                            var clienteMLabRaizCuenta = requestjson.createClient(urlMLabRaiz + "Cuentas?" + apiKey);
                            clienteMLabRaizCuenta.post('', JSON.parse(jsonCuenta), function(errCuenta, resCuenta, bodyCuenta){
                              if(!errCuenta){
                                res.status(201).send({error: false, mensaje:"Usuario registrado correctamente."});
                              }else {
                                res.status(404).send({error: true, mensaje:"Error al intentar registrar la cuenta del cliente."});
                              }
                            });
                      }else{
                        res.status(404).send({error: true, mensaje:"Error al intentar registrar al cliente."});
                      }
                    });
                  })//bcrypt.hash(
                }
                else{
                  res.status(404).send({error: true, mensaje:"Error al intentar el idCliente."});
                }
              })//clienteMLabRaizIncrementa.get
            }
          }
        }) //clienteMLabRaizUsuario.get
      }
    }
  }) //clienteMLabRaiz.get
}// IF valida campos
}) // app.post


//GET Obtener cuentas de un Usuario
app.get('/v1/usuarios/:idCliente', function(req, res){
  var query = 'q={"idCliente":' + req.params.idCliente +'}';

  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Cuentas?" + apiKey + "&" + query);
  clienteMLabRaiz.get('', function(err, resM, body){
    if(!err){
      if(body.length > 0){
        res.status(200).send(body);
      }
      else{
        res.status(400).send({error: true, mensaje: 'No se encontraron cuentas para el usuario'});
      }
    }
  })
})

//POST Guardar cuenta
app.post('/v1/cuentas/:idCliente', function(req, res){
  var idCliente = req.params.idCliente;
  var query = 'q={"idCuenta":' + req.body.idCuenta +'}';

  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Cuentas?" + apiKey + "&" + query);
  clienteMLabRaiz.get('', function(errCuenta, resCuenta, bodyCuenta){
    if(!errCuenta){
      if(bodyCuenta.length >= 1){
        res.status(400).send({error: true, "mensaje":"La cuenta proporcionada ya está registrada."});
      }else{
        var jsonCuenta = '{"idCuenta":' + req.body.idCuenta +', "monto":' + req.body.monto +', "idCliente":' + idCliente + '}';

        var clienteMLabRaizCuenta = requestjson.createClient(urlMLabRaiz + "Cuentas?" + apiKey);
        clienteMLabRaizCuenta.post('', JSON.parse(jsonCuenta), function(errCuenta2, resCuenta2, bodyCuenta2){
          if(!errCuenta2){
            res.status(201).send({error:false, "mensaje":"Se registró la cuenta con éxito"});
        }
        })
      }
    }
  })
})

//POST - Crea un movimiento a una cuenta asociada
app.post('/v1/clientes/:idcliente/movimientos', function(req, res){
  var idcliente = req.params.idcliente;
  var idcuenta = req.body.idCuenta;
  var importe = req.body.importeAlta;
  var tipo = req.body.tipoMovimientoAlta;
  var query = 'q={"idCliente":' + idcliente +', "idCuenta": ' + idcuenta +'}';

  clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Cuentas?" + apiKey + "&" + query);
  clienteMLabRaiz.get('', function(err, resM, body){
    if(!err){
      if(body.length == 1){
        var now = new Date();
        var fechaMovimiento = dateFormat(now, "yyyy-mm-dd HH:MM:ss");
        var idMovimiento = body[0].movimientos == null ? 1 : (body[0].movimientos.length+1);
        var movimiento = {
            id: idMovimiento,
            importe: importe,
            tipo: tipo,
            fecha: fechaMovimiento
        };

        var data = [];
        data = body[0].movimientos == null ? [] : body[0].movimientos;
        data.push(movimiento)
        body[0].movimientos = data;
        var jsonCuentaMovimiento = body[0];

        var clienteMLabRaizMovimiento = requestjson.createClient(urlMLabRaiz + "Cuentas?" + apiKey);
        clienteMLabRaizMovimiento.post('', jsonCuentaMovimiento, function(err, resM, bodyCuentaMovimiento){
          res.status(200).send(bodyCuentaMovimiento);
        });
      }
      else{
        res.status(400).send({error: true, mensaje:'Error al generar el movimiento'});
      }
    }
  })
})

//DELETE eliminar la informacion de un cuenta
app.delete('/v1/usuarios/:idCliente/cuentas/:idCuenta', function (req, res, next) {
    let idCuenta = req.params.idCuenta;
    let idCliente = req.params.idCliente;
    var queryCuenta = 'q={"idCuenta":' + idCuenta +', "idCliente":'+ idCliente +'}';
    clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "Cuentas?" + apiKey + "&" + queryCuenta);
    clienteMLabRaiz.get('', function(err, resM, body){
      if(!err){
        if(body.length >= 1){
          //res.status(200).send({error: false, mensaje:"Se encontro la información de la cuenta."});
          var deleteCuenta = '{"idCuenta":' + idCuenta +'}';
          //var deleteCuenta = '{"idCuenta": 102}';
          var clienteMLabRaizCuenta = requestjson.createClient(urlMLabRaiz + "Cuentas?" + apiKey);
          clienteMLabRaizCuenta.delete('',deleteCuenta, function(err, resM, body){
          //clienteMLabRaizCuenta.delete('', async(errCuenta, resCuenta)=>{
            if(!err){
              res.status(200).send({error:false, "mensaje":"Se eliminó la cuenta con éxito."});
            }else {
              res.status(400).send({error:true, "mensaje":"No se logro eliminar la información de la cuenta."});
            }
          })
        }else {
          res.status(400).send({error: true, mensaje:"No se encontro información de la cuenta."});
        }
      }else {
        res.status(400).send({error: true, mensaje: "" + body});
      }
    })

 });

// PUT Actualizar la informacion de un usuario
app.put('/v1/usuarios/:id',function(req, res, next){
  if (!req.body.email || !req.body.password){
    res.status(404).send('El dato del Correo y Contraseña son requeridos.');
  }else {
    if (req.body.email === ''){
      res.status(404).send('El Correo del usuario no puede ser vacio.');
    }else{
      let id =  req.params.id;
      usuarioMLab.put('{"_id": ' + id +'}', req.body, function (err, resM, body){
        res.status(200).send(body);
    })
    }
  }
})
